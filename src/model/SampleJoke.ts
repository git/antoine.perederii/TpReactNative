/**
 * @file SampleJoke.ts
 * @brief Définition de la classe SampleJoke.
 */

import { Joke } from "./Joke";

/**
 * @class
 * @brief Classe représentant une blague d'échantillon.
 * @extends Joke
 */
export class SampleJoke extends Joke {
    private _id: number;

    /**
     * @brief Constructeur de la classe SampleJoke.
     * @param {number} id - L'identifiant de la blague d'échantillon.
     * @param {string} type - Le type de la blague.
     * @param {string} setup - La partie préliminaire de la blague.
     * @param {string} punchline - La chute de la blague.
     * @param {string} image - L'URL de l'image associée à la blague.
     */
    constructor(id: number, type: string, setup: string, image: string, punchline: string = "") {
        super(type, setup, punchline, image); // Assuming Joke class has these properties
        this._id = id;
    }


    /**
     * @brief Obtient l'identifiant de la blague d'échantillon.
     * @return {number} L'identifiant de la blague d'échantillon.
     */
    get id(): number {
        return this._id;
    }

    /**
     * @brief Modifie l'identifiant de la blague d'échantillon.
     * @param {number} id - Le nouvel identifiant de la blague d'échantillon.
     */
    set id(id: number) {
        this._id = id;
    }
}