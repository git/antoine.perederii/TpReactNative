import { Category } from './Category';
import {describe, expect, it} from "@jest/globals";

describe('Category Class Constructor', () => {
    it('should create a new Category object', () => {
        const category = new Category('name', 5);
        expect(category).toBeDefined();
        expect(category.name).toBe('name');
        expect(category.number).toBe(5);
    });
});

describe('Category Class Accessors', () => {
    it('should set and get the name correctly', () => {
        const category = new Category('name', 5);
        category.name = 'newName';
        expect(category.name).toBe('newName');
    });

    it('should set and get the number correctly', () => {
        const category = new Category('name', 5);
        category.number = 10;
        expect(category.number).toBe(10);
    });
});
