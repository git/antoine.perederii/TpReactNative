/**
 * @file CustomJoke.ts
 * @brief Définition de la classe CustomJoke.
 */

import { Joke } from './Joke';

/**
 * @class
 * @brief Représente une blague personnalisée avec un identifiant unique.
 * @extends Joke
 */
export class CustomJoke extends Joke {
    private _id: string;

    /**
     * @brief Constructeur de la classe CustomJoke.
     * @param {string} id - L'identifiant unique de la blague.
     * @param {string} type - Le type de la blague.
     * @param {string} setup - La partie préliminaire de la blague.
     * @param {string} punchline - La chute de la blague.
     * @param {string} image - L'URL de l'image associée à la blague.
     */
    constructor(id: string, type: string, setup: string, image: string, punchline: string = "") {
        super(type, setup, punchline, image); // Assuming Joke class has these properties
        this._id = id;
    }

    /**
     * @brief Obtient l'identifiant de la blague.
     * @return {string} L'identifiant de la blague.
     */
    get id(): string {
        return this._id;
    }

    /**
     * @brief Modifie l'identifiant de la blague.
     * @param {string} id - Le nouvel identifiant de la blague.
     */
    set id(id: string) {
        this._id = id;
    }
}