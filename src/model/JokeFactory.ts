/**
 * @file JokeFactory.ts
 * @brief Définition de la classe JokeFactory.
 */

import { CustomJoke } from "./CustomJoke";
import { SampleJoke } from "./SampleJoke";

/**
 * @class
 * @brief Fabrique de blagues permettant de créer des instances de blagues à partir de données JSON.
 */
export class JokeFactory {
    /**
     * @brief Crée des instances de blagues personnalisées à partir d'un tableau JSON.
     * @param {string} jsonArray - Tableau JSON représentant les blagues personnalisées.
     * @return {CustomJoke[]} Tableau d'instances de blagues personnalisées.
     */
    public static createCustomJokes(jsonArray: string): CustomJoke[] {
        const jsonObjects: any[] = JSON.parse(jsonArray);
        const customJokes: CustomJoke[] = [];

        for (const jsonObject of jsonObjects) {
            const customJoke: CustomJoke = new CustomJoke(
                jsonObject.id,
                jsonObject.type,
                jsonObject.setup,
                jsonObject.punchline,
                jsonObject.image
            );
            customJokes.push(customJoke);
        }

        return customJokes;
    }

    /**
     * @brief Crée des instances de blagues d'échantillon à partir d'un tableau JSON.
     * @param {string} jsonArray - Tableau JSON représentant les blagues d'échantillon.
     * @return {SampleJoke[]} Tableau d'instances de blagues d'échantillon.
     */
    public static createSampleJokes(jsonArray: string): SampleJoke[] {
        const jsonObjects: any[] = JSON.parse(jsonArray);
        const sampleJokes: SampleJoke[] = [];

        for (const jsonObject of jsonObjects) {
            const sampleJoke: SampleJoke = new SampleJoke(
                jsonObject.id,
                jsonObject.type,
                jsonObject.setup,
                jsonObject.punchline,
                jsonObject.image
            );
            sampleJokes.push(sampleJoke);
        }

        return sampleJokes;
    }
}