/**
 * @file Category.ts
 * @brief Définition de la classe Catégory.
 */

/**
 * @class
 * @brief Représente une catégorie nom.
 */
export class Category {
    private _name: string;
    private _number: number;

    /**
     * @brief Constructeur de la classe Category.
     * @param {string} name - Le nom de la catégorie.
     * @param {number} number - Le nombre de la catégorie.
     */
    constructor(name: string, number: number) {
        this._name = name;
        this._number = number;
    }

    /**
     * @brief Obtient le nom de la catégorie.
     * @return {string} Le nom de la catégorie.
     */
    get name(): string {
        return this._name;
    }

    /**
     * @brief Obtient le nombre de la catégorie.
     * @return {number} Le nombre de la catégorie.
     */
    get number(): number {
        return this._number;
    }

    /**
     * @brief Modifie le nom de la catégorie.
     * @param {string} name - Le nom de la categorie.
     */
    set name(name: string) {
        this._name = name;
    }

    /**
     * @brief Modifie le nombre de la catégorie.
     * @param {number} number - Le nombre de la catégorie.
     */
    set number(number: number) {
        this._number = number;
    }
}