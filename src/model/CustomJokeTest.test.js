import { CustomJoke } from './CustomJoke';
import {describe, expect, test} from "@jest/globals"; // Remplacez le chemin par le bon chemin

describe('CustomJoke Class', () => {
    const id = '1';
    const type = 'Custom';
    const setup = 'Why did the developer go broke?';
    const image = 'custom-image-url';
    const punchline = 'Because he used up all his cache';

    test('creates a new instance of CustomJoke', () => {
        const customJoke = new CustomJoke(id, type, setup, image, punchline);
        expect(customJoke).toBeInstanceOf(CustomJoke);
        expect(customJoke.id).toEqual(id);
        expect(customJoke.type).toEqual(type);
        expect(customJoke.setup).toEqual(setup);
        expect(customJoke.punchline).toEqual(punchline);
        expect(customJoke.image).toEqual(image);
    });

    test('updates CustomJoke properties', () => {
        const newSetup = 'Why do programmers prefer dark mode?';
        const newImage = 'new-custom-image-url';
        const newPunchline = 'Because light attracts bugs';
        const customJoke = new CustomJoke(id, type, setup, image, punchline);

        customJoke.setup = newSetup;
        customJoke.image = newImage;
        customJoke.punchline = newPunchline;

        expect(customJoke.setup).toEqual(newSetup);
        expect(customJoke.image).toEqual(newImage);
        expect(customJoke.punchline).toEqual(newPunchline);
    });
});
