const { Joke } = require('./Joke');
const {expect, it, beforeEach, describe} = require("@jest/globals");

// Mock class extending the abstract Joke class
class MockJoke extends Joke {
    constructor(type, setup, punchline, image) {
        super(type, setup, punchline, image);
    }
}

// Test the Joke class
describe('Joke Class', () => {
    let joke;

    beforeEach(() => {
        joke = new MockJoke('type', 'setup', 'punchline', 'image');
    });

    // Test the constructor
    it('should create a new Joke object', () => {
        expect(joke).toBeDefined();
        expect(joke.type).toBe('type');
        expect(joke.setup).toBe('setup');
        expect(joke.punchline).toBe('punchline');
        expect(joke.image).toBe('image');
    });

    // Test the summary() method
    it('should return a summary of the joke', () => {
        expect(joke.summary()).toBe('punchline');
    });

    // Test the description() method
    it('should return a textual description of the joke', () => {
        expect(joke.description()).toBe('type, punchline');
    });

    // Test setting and getting the type
    it('should set and get the type correctly', () => {
        joke.type = 'newType';
        expect(joke.type).toBe('newType');
    });

    // Test setting and getting the setup
    it('should set and get the setup correctly', () => {
        joke.setup = 'newSetup';
        expect(joke.setup).toBe('newSetup');
    });

    // Test setting and getting the punchline
    it('should set and get the punchline correctly', () => {
        joke.punchline = 'newPunchline';
        expect(joke.punchline).toBe('newPunchline');
    });

    // Test setting and getting the image
    it('should set and get the image correctly', () => {
        joke.image = 'newImage';
        expect(joke.image).toBe('newImage');
    });
});
