/**
 * @file Joke.ts
 * @brief Définition de la classe abstraite Joke.
 */

/**
 * @class
 * @brief Classe abstraite représentant une blague.
 */
export abstract class Joke {
    private _type: string;
    private _setup: string;
    private _punchline: string;
    private _image: string;

    /**
     * @brief Constructeur de la classe Joke.
     * @param {string} type - Le type de la blague.
     * @param {string} setup - La partie préliminaire de la blague.
     * @param {string} punchline - La chute de la blague.
     * @param {string} image - L'URL de l'image associée à la blague.
     */
    constructor(type: string, setup: string, punchline: string, image: string) {
        this._type = type;
        this._setup = setup;
        this._punchline = punchline;
        this._image = image;
    }

    /**
     * @brief Obtient le type de la blague.
     * @return {string} Le type de la blague.
     */
    get type() {
        return this._type;
    }

    /**
     * @brief Obtient la partie préliminaire de la blague.
     * @return {string} La partie préliminaire de la blague.
     */
    get setup(): string {
        return this._setup;
    }

    /**
     * @brief Obtient la chute de la blague.
     * @return {string} La chute de la blague.
     */
    get punchline(): string {
        return this._punchline;
    }

    /**
     * @brief Obtient l'URL de l'image associée à la blague.
     * @return {string} L'URL de l'image associée à la blague.
     */
    get image(): string {
        return this._image;
    }

    /**
     * @brief Modifie le type de la blague.
     * @param {string} theType - Le nouveau type de la blague.
     */
    set type(theType: string) {
        this._type = theType;
    }

    /**
     * @brief Modifie la partie préliminaire de la blague.
     * @param {string} theSetup - La nouvelle partie préliminaire de la blague.
     */
    public set setup(theSetup: string) {
        this._setup = theSetup;
    }

    /**
     * @brief Modifie la chute de la blague.
     * @param {string} thePunchline - La nouvelle chute de la blague.
     */
    public set punchline(thePunchline: string) {
        this._punchline = thePunchline;
    }

    /**
     * @brief Modifie l'URL de l'image associée à la blague.
     * @param {string} theImage - Le nouvel URL de l'image associée à la blague.
     */
    public set image(theImage: string) {
        this._image = theImage;
    }

    /**
     * @brief Obtient un résumé de la blague.
     * @return {string} Un résumé de la blague.
     */
    public summary(): string {
        if(this.punchline.length <= 25){
            return this.punchline;
        }
        return this.punchline.substring(0, 24) + "...";
    }

    /**
     * @brief Obtient une description textuelle de la blague.
     * @return {string} Une description textuelle de la blague.
     */
    public description(): string {
        return this.type + ", " + this.summary();
    }
}