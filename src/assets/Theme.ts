import {Theme} from "@react-navigation/native";

export const indigoColor = "rgba(14, 14, 44, 1)";
export const purpleColor = "rgba(74, 74, 104, 1)";
export const darksalmonColor = "rgba(233, 150, 122, 1)";
export const greyColor = "rgba(140, 140, 161, 1)";
export const whiteColor = "rgba(239, 239, 253, 1)";


export const LightTheme: Theme = {
    dark: false,
    colors: {
        primary: darksalmonColor,
        background: whiteColor,
        card: greyColor,
        text: "black",
        border: whiteColor,
        notification: 'rgb(255, 59, 48)',
    },
};

export const DarkTheme: Theme = {
    dark: true,
    colors: {
        primary: darksalmonColor,
        background: purpleColor,
        card: indigoColor,
        text: whiteColor,
        border: greyColor,
        notification: 'rgb(255, 69, 58)',
    },
};