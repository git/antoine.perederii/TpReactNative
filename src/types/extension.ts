export {};

declare global {
    interface Array<T> {
        displayDescription(): string[];
    }
}

Array.prototype.displayDescription = function () {
    return this.map((item) => item.description() )
};