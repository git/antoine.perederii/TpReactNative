/**
 * @file CustomJokeStub.ts
 * @brief Exemple d'utilisation de la classe JokeFactory pour créer des blagues personnalisées.
 */

import { JokeFactory } from '../../model/JokeFactory';

/**
 * @brief Stub de blagues personnalisées créées à l'aide de la classe JokeFactory.
 * @constant
 * @type {CustomJoke[]}
 */
export const customJokeStub = JokeFactory.createCustomJokes('[{"id":"premier", "type":"custom", "setup":"one", "punchline":"y\'en a pas", "image":"https://placekitten.com/200/300"},{"id":"deuxieme", "type":"custom", "setup":"two","punchline":"y\'en a pas", "image":"https://placekitten.com/200/300"},{"id":"troisieme", "type":"Default", "setup":"three","punchline":"y\'en toujours a pas ;)", "image":"https://placekitten.com/200/300"},{"id":"quatrieme", "type":"custom bro", "setup":"four","punchline":"y\'en toujours toujours ap", "image":"https://placekitten.com/200/300"}]');