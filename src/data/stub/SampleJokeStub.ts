/**
 * @file SampleJokeStub.ts
 * @brief Exemple d'utilisation de la classe JokeFactory pour créer des blagues simples.
 */

import { JokeFactory } from '../../model/JokeFactory';

/**
 * @brief Stub de blagues simples créées à l'aide de la classe JokeFactory.
 * @constant
 * @type {SampleJoke[]}
 */
export const sampleJokeStub = JokeFactory.createSampleJokes('[{"id":1, "type":"custom", "setup":"one", "punchline":"y\'en a pas", "image":"https://placekitten.com/200/300"},{"id":2, "type":"custom", "setup":"two","punchline":"y\'en a pas", "image":"https://placekitten.com/200/300"}]');