import { SafeAreaView } from 'react-native'
import {StyleSheet} from 'react-native';
import NavigationBar from "./navigation/NavigationBar";
import {indigoColor} from "./assets/Theme";
import store from "./redux/store";
import React from "react";
import {Provider} from "react-redux";

export default function App() {
    return (
        <Provider store={store}>
            <SafeAreaView style={{flex: 0, backgroundColor: 'darksalmon'}}/>
            <SafeAreaView style={styles.container}>
                <NavigationBar />
            </SafeAreaView>
        </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: indigoColor
    },
});