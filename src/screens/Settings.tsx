import '../types/extension';
import {Image, StyleSheet, Switch, Text, View} from "react-native";
import {
    darksalmonColor, DarkTheme,
    whiteColor
} from "../assets/Theme";
import React from "react";
import {DefaultTheme, useTheme} from "@react-navigation/native";
import {storeTheme} from "../redux/thunk/ThemeThunk";

export default function Catalogue() {
    const light_mode = require("../assets/light_mode.png")
    const dark_mode = require("../assets/dark_mode.png")
    const [isDark, setDark] = React.useState(false)

    const toggleTheme = () => {
        setDark(previousState => {
            const theme = !previousState;
            const newTheme = theme ? DarkTheme : DefaultTheme;
            storeTheme(newTheme);
            return theme;
        });
    };

    const styles = themeSettings();

    return (
        <View style={styles.container}>
            <View style={styles.topText}>
                <Text style={styles.title}>Réglages</Text>
                <View style={styles.switchMode}>
                    <View style={styles.textContainer}>
                        <Image
                            source={isDark? dark_mode: light_mode}
                            style={styles.imageButton} />
                        <Text style={styles.darkModeText}>Dark Mode</Text>
                    </View>
                    <Switch
                        value={isDark}
                        onValueChange={toggleTheme}
                        style={styles.switchMode}
                        trackColor={{false: darksalmonColor, true: darksalmonColor}}
                        thumbColor={whiteColor} />
                </View>
            </View>
        </View>
    );
};

export function themeSettings() {
    const theme = useTheme();
    const colors = theme.colors;

    const styles= StyleSheet.create({

        container: {
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 10,
            paddingRight: 10,
            flex: 1,
            justifyContent: 'center',
            backgroundColor: colors.background,
            flexDirection: 'column',
        },
        topText: {
            flex: 1,
        },
        title: {
            padding: 10,
            fontSize: 20,
            color: colors.text,
            fontWeight: 'bold'
        },
        imageButton : {
            width: 30,
            height:30,
            alignSelf : "center",
            tintColor: colors.primary,
        },
        switchMode: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: colors.card,
            padding: 20,
            margin: 10,
        },
        darkModeText: {
            color: colors.text,
            fontSize: 20,
            marginLeft: 10,
            paddingTop: 5,
        },
        textContainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
        }
    });
    return styles;
}