import { customJokeStub } from '../data/stub/CustomJokeStub';
import { sampleJokeStub } from '../data/stub/SampleJokeStub';
import JokeItems from "../components/JokeItems";
import '../types/extension';
import {useDispatch, useSelector} from "react-redux";
import {Image, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {darksalmonColor, greyColor, indigoColor, purpleColor, whiteColor} from "../assets/Theme";
import {getCustomJokesList, getSampleJokesList} from "../redux/thunk/GetThunk";
import React, {useEffect} from "react";
import {postCustomJoke} from "../redux/thunk/PostThunk";
import {useTheme} from "@react-navigation/native";

export default function AddScreen() {
    // @ts-ignore
    const dispatch = useDispatch();
    const MAX_LENGTH = 10;
    const [joke, onChangeJoke] = React.useState('');
    const [downgrade, onChangeDowngrade] = React.useState('');
    const [category, onChangeCategory] = React.useState('');

    const clearFields = () => {
        onChangeCategory('');
        onChangeJoke('');
        onChangeDowngrade('');
    }
    const handleCreate = () => {
        // @ts-ignore
        dispatch(postCustomJoke(joke, downgrade, category));
        // @ts-ignore
        dispatch(getCustomJokesList());

        clearFields();
    };

    const styles = themeSettings()

    return (
        <View style={styles.container}>
            <Text style={styles.text}>Blague</Text>
            <TextInput
                style={styles.textInput}
                onChangeText={onChangeJoke}
                value={joke}
                placeholder="Inserez votre blague"
                placeholderTextColor={whiteColor}
                scrollEnabled={true}
                autoCorrect={true}
                multiline={true}
                numberOfLines={10}
                cursorColor={indigoColor}
            />
            <Text style={styles.text}>Chute de la blague</Text>
            <TextInput
                style={styles.textInput}
                onChangeText={onChangeDowngrade}
                value={downgrade}
                multiline={true}
                placeholder="Inserez votre blague"
                placeholderTextColor={whiteColor}
                scrollEnabled={true}
                autoCorrect={true}
                numberOfLines={10}
                cursorColor={indigoColor}
            />
            <Text style={styles.text}>Catégorie</Text>
            <TextInput
                style={styles.textInput}
                onChangeText={onChangeCategory}
                value={category}
                placeholder="Inserez votre blague"
                placeholderTextColor={whiteColor}
                scrollEnabled={true}
                autoCorrect={true}
                maxLength={10}
                cursorColor={indigoColor}
            />
            <View style={styles.viewCounter}>
                <Text style={styles.textSize}>{category.length}/{MAX_LENGTH}</Text>
            </View>

            <TouchableOpacity style={styles.createButton} onPress={handleCreate}>
                <Text style={styles.createTextButton} >CRÉER</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.eraseButton} onPress={clearFields}>
                <Text style={styles.eraseTextButton} >EFFACER</Text>
            </TouchableOpacity>
        </View>
    )
};


export function themeSettings() {
    const theme = useTheme();
    const colors = theme.colors;
    const styles = StyleSheet.create({
        container: {
            backgroundColor: colors.background,
            width: "100%",
            height: "100%",
        },
        textInput: {
            backgroundColor: colors.card,
            color: colors.text,
            width: "90%",
            alignSelf: "center",
            minHeight: 100
        },
        eraseButton: {
            borderRadius: 5,
            alignItems: "center",
            backgroundColor: colors.primary,
            height: 50,
            margin: 10
        },
        createButton: {
            borderRadius: 5,
            alignItems: "center",
            backgroundColor: colors.primary,
            height: 50,
            margin: 10,
            marginTop: 30
        },
        createTextButton: {
            margin: 10,
            textAlign: "center",
            fontWeight: "700",
            color: colors.text,
        },
        eraseTextButton: {
            margin: 10,
            textAlign: "center",
            fontWeight: "700",
            color: colors.text,
        },
        text: {
            color: colors.text,
            paddingBottom: 10,
            paddingTop: 25,
            marginLeft: 19,
            fontSize: 20,
        },
        textSize: {
            paddingTop: 15,
            marginRight: 19,
            fontSize: 12,
            color: colors.text,
        },
        viewCounter: {
            alignItems: 'flex-end',
            bottom: 10,
            right: 10,
        }
    })
    return styles
}