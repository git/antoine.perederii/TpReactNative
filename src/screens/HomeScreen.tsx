import {Image, StyleSheet, Text, View} from 'react-native';
import '../types/extension';
import {darksalmonColor, purpleColor, whiteColor} from "../assets/Theme";
import JokesHomeSquare from "../components/JokesHomeSquare";
import Categs from "../components/Categs";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {getCategoriesList, getLastSampleJokesList} from "../redux/thunk/GetThunk";
import {useTheme} from "@react-navigation/native";
import styleToBarStyle from "expo-status-bar/build/styleToBarStyle";

export default function Catalogue() {
    // @ts-ignore
    const allJokes = useSelector(state => state.sampleReducer.jokes);
    // @ts-ignore
    const allCategories = useSelector(state => state.categoryReducer.categories);
    const dispatch = useDispatch();

    useEffect(() => {
        const loadJokes = async () => {
            // @ts-ignore
            await dispatch(getLastSampleJokesList());
            // @ts-ignore
            await dispatch(getCategoriesList());
        };
        loadJokes();
    }, [dispatch]);

    const styles = themeSettings()

    return (
        <>
            <View style={styles.container}>
                <View style={styles.top}>
                    <Image style={styles.cat} source={require('../assets/logo.png')}/>
                    <Text style={styles.textAccueil}>Chat C'est Drole</Text>
                </View>
                <View style={styles.Jokes}>
                    <Text style={styles.textLastJokes}>Dernières blagues</Text>
                    <JokesHomeSquare jokes={allJokes} />
                </View>
                <View style={styles.bestCateg}>
                    <View style={styles.horizBestCateg}>
                        <Text style={styles.textBestCateg}>Top Categories</Text>
                        <Image source={require("../assets/fire_icon.png")}/>
                    </View>
                    <View style={styles.horizChip}>
                        <Categs categories={allCategories}/>
                    </View>
                </View>
            </View>
        </>
    )
};

export function themeSettings() {
    const theme = useTheme();
    const colors = theme.colors;
    const styles = StyleSheet.create({
        container: {
            backgroundColor: colors.background,
            flex: 1,
        },
        top: {
            alignItems: "center",
        },
        cat: {
            height: 160,
            width: 160
        },
        textAccueil: {
            fontSize: 25,
            color: colors.text,
            fontWeight: "bold",
        },
        Jokes: {
            display: "flex",
            flexDirection: "column",
            marginTop: 50,
            marginLeft: 20,
        },
        scrollList: {
            alignItems: "center"
        },
        textLastJokes: {
            color: colors.border,
            fontSize: 20,
            fontWeight: "bold",
        },
        bestCateg: {
            marginTop: 30,
            marginLeft: 20,
        },
        horizBestCateg: {
            flexDirection: "row",
        },
        textBestCateg: {
            color: colors.text,
            fontSize: 20,
            fontWeight: "bold",
        },
        horizChip: {
            flexDirection: "row",
            marginTop: 30,
        }
    });
    return styles
}