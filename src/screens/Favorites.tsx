import JokeItems from "../components/JokeItems";
import '../types/extension';
import {Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {darksalmonColor, purpleColor, whiteColor} from "../assets/Theme";
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import {getCustomJokesList, getSampleJokesList} from "../redux/thunk/GetThunk";
import {useTheme} from "@react-navigation/native";

export default function Favorites() {
    // @ts-ignore
    const sampleJokes = useSelector(state => state.sampleReducer.jokes);
    // @ts-ignore
    const customJokes = useSelector(state => state.customReducer.customJokes);
    const [joke, setJoke] = useState([])
    const dispatch = useDispatch();
    const eye = require("../assets/eye_icon.png")
    const hideEye = require("../assets/eye_off_icon.png")

    const [showCustomJoke, setCustomJoke] = useState(false); // état local pour contrôler la visibilité de la description
    const toggleDescription = () => {
        setCustomJoke(!showCustomJoke);
    };

    useEffect(() => {
        if(!showCustomJoke) {
            const loadSamplesJokes = async () => {
                // @ts-ignore
                await dispatch(getFavorites());
            };
            loadSamplesJokes();
            setJoke(sampleJokes)
        } else {
            const loadCustomJokes = async () => {
                // @ts-ignore
                await dispatch(getCustomJokesList());
            };
            loadCustomJokes();
            setJoke(customJokes)
        }
    }, [dispatch, customJokes, sampleJokes]);

    const styles = themeSettings()

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.columnContainer}>
                <Text style={styles.TextButton}>Afficher les exemples</Text>
                <TouchableOpacity style={styles.Button} onPress={toggleDescription}>
                    <View style={styles.jokeTypeContainer}>
                        <Image
                            source={showCustomJoke ? hideEye : eye}
                            style={styles.imageButton}
                        />
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                <JokeItems jokes={joke}/>
            </View>
        </SafeAreaView>
    )
};


export function themeSettings() {
    const theme = useTheme();
    const colors = theme.colors;
    const styles = StyleSheet.create({
        container: {
            backgroundColor: colors.background,
            flex: 1,
        },
        Button: {
            borderRadius: 5,
            backgroundColor: colors.background,
            height: 40,
            width: 60,
            flexDirection: "row"
        },
        jokeTypeContainer: {
            display: "flex",
            flex: 1,
            flexDirection: "row",
            alignItems: "center"
        },
        imageButton: {
            margin: 10,
            width: 40,
            height: 30,
            top: 5,
            alignSelf: "center",
            backgroundColor: "none",
            tintColor: colors.notification,
            justifyContent: "center",
            alignItems: "center",
        },
        TextButton: {
            fontSize: 18,
            color: colors.notification,
            textAlign: 'center',
            fontWeight: 'bold',
            margin: 10,
        },
        columnContainer: {
            marginLeft: 20,
            marginRight: 20,
            width: '90%',
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
        }
    });
    return styles
}