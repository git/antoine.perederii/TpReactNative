import {configureStore} from '@reduxjs/toolkit'
import sampleReducer from "./reducers/SampleReducer";
import categoryReducer from "./reducers/CategoryReducer";
import customReducer from "./reducers/CustomReducer";
import themeReducer from "./reducers/DarkModeReducer"
import favoriteReducer from "./reducers/FavoriteReducer";

// Reference here all your application reducers
const reducer = {
    sampleReducer: sampleReducer,
    categoryReducer: categoryReducer,
    customReducer: customReducer,
    theme: themeReducer,
    favorite: favoriteReducer
}

// @ts-ignore
const store = configureStore({
    reducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false
        })
},);
export type AppState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export default store;