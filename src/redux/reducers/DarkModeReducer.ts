import {Action, ActionType} from "../actions/DarkModeAction";

interface State {
    theme: String
}

const initialState = {
    theme: 'dark'
}

const themeReducer = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case ActionType.SET_THEME:
            // @ts-ignore
            return {...state, theme: action.payload};
        default:
            return state;
    }
}

export default themeReducer;