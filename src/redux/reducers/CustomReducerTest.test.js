import customReducer from './CustomReducer';
import { ActionType } from '../actions/CustomJoke';
import { CustomJoke } from '../../model/CustomJoke';
import {describe, expect, it} from "@jest/globals";

describe('Custom Reducer', () => {
    it('should return the initial state', () => {
        expect(customReducer(undefined, {})).toEqual({
            customJokes: [],
            customJoke: null,
            jokeId: ''
        });
    });

    it('should handle POST_CUSTOM_JOKE', () => {
        const customJoke: CustomJoke = new CustomJoke('1', 'pun', 'Setup', 'https://example.com/image.jpg', 'Punchline');

        const action = {
            type: ActionType.POST_CUSTOM_JOKE,
            payload: customJoke
        };

        expect(customReducer(undefined, action)).toEqual({
            customJokes: [],
            customJoke: customJoke,
            jokeId: ''
        });
    });
});
