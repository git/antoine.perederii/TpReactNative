import {CustomJoke} from "../../model/CustomJoke";
import {Action, ActionType} from "../actions/CustomJoke";

interface State {
    customJokes: CustomJoke[];
    customJoke?: CustomJoke;
    jokeId: string;
}

const initialState = {
    customJokes: [],
    customJoke: null,
    jokeId: ''
}

const customReducer = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case ActionType.POST_CUSTOM_JOKE:
            return {...state, customJoke: action.payload};
        case ActionType.FETCH_CUSTOM_JOKES:
            return {...state, customJokes: action.payload};
        case ActionType.FETCH_JOKES_BY_ID:
            return {...state, customJoke: action.payload};
        case ActionType.DELETE_CUSTOM_JOKE:
            return {...state, jokeId: action.payload};
        default:
            return state;
    }
}

export default customReducer;