import categoryReducer from './CategoryReducer';
import { ActionType as CategoryActionType } from '../actions/CategoryAction';
import { Category } from '../../model/Category';
import {describe, expect, it} from "@jest/globals";

describe('Category Reducer', () => {

    it('should handle FETCH_CATEGORIES', () => {
        const categories = [
            new Category('Category 1', 1),
            new Category('Category 2', 2)
        ];

        const action = {
            type: CategoryActionType.FETCH_CATEGORIES,
            payload: categories
        };

        expect(categoryReducer(undefined, action)).toEqual({
            categories: categories
        });
    });
});