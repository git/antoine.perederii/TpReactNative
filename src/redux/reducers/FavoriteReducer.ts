import {Action, ActionType} from "../actions/FavoriteAction";
import {CustomJoke} from "../../model/CustomJoke";
import {SampleJoke} from "../../model/SampleJoke";

interface State {
    joke: CustomJoke | SampleJoke
}

const initialState = {
    joke: new CustomJoke('', '', '', '', '')
}

const favoriteReducer = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case ActionType.ADD_FAVORITE:
            // @ts-ignore
            return {...state, joke: action.payload};
        case ActionType.REMOVE_FAVORITE:
            // @ts-ignore
            return {...state, joke: action.payload};
        default:
            return state;
    }
}

export default favoriteReducer;
