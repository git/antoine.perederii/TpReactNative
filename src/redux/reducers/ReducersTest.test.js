// Importez les réducteurs et les types d'action appropriés
import categoryReducer from './CategoryReducer';
import customReducer from './CustomReducer';
import themeReducer from './DarkModeReducer';
import sampleReducer from './SampleReducer';
import { ActionType as CategoryActionType } from '../actions/CategoryAction';
import { ActionType as CustomActionType } from '../actions/CustomJoke';
import { ActionType as ThemeActionType } from '../actions/DarkModeAction';
import { ActionType as SampleActionType } from '../actions/SampleAction';
import { Category } from '../../model/Category';
import { CustomJoke } from '../../model/CustomJoke';
import { SampleJoke } from '../../model/SampleJoke';
import {describe, expect, it} from "@jest/globals";

// Tester categoryReducer
describe('Category Reducer', () => {
    it('should return the initial state', () => {
        expect(categoryReducer(undefined, {})).toEqual({
            categories: []
        });
    });

    // Ajouter d'autres tests pour categoryReducer si nécessaire...
});

// Tester customReducer
describe('Custom Reducer', () => {
    it('should return the initial state', () => {
        expect(customReducer(undefined, {})).toEqual({
            customJokes: [],
            customJoke: null,
            jokeId: ''
        });
    });

    // Ajouter d'autres tests pour customReducer si nécessaire...
});

// Tester themeReducer
describe('Theme Reducer', () => {
    it('should return the initial state', () => {
        expect(themeReducer(undefined, {})).toEqual({
            theme: 'dark'
        });
    });

    // Ajouter d'autres tests pour themeReducer si nécessaire...
});

// Tester sampleReducer
describe('Sample Reducer', () => {
    it('should return the initial state', () => {
        expect(sampleReducer(undefined, {})).toEqual({
            jokes: [],
            favoriteJokes: [],
            joke: null,
        });
    });

    // Ajouter d'autres tests pour sampleReducer si nécessaire...
});
