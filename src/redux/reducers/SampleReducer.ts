import {CustomJoke} from "../../model/CustomJoke";
import {SampleJoke} from "../../model/SampleJoke";
import {Action, ActionType} from "../actions/SampleAction";
import {Category} from "../../model/Category";

interface State {
    jokes: SampleJoke[];
    favoriteJokes: SampleJoke[];
    joke? : SampleJoke;
}

const initialState = {
    jokes: [],
    favoriteJokes: [],
    joke:null,
}

const sampleReducer = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case ActionType.FETCH_JOKES:
            // @ts-ignore
            return {...state, jokes: action.payload};
        case ActionType.FETCH_JOKES_BY_ID:
            // @ts-ignore
            return {...state, joke: action.payload}
        default:
            return state;
    }
}

export default sampleReducer;