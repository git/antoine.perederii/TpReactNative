import themeReducer from './DarkModeReducer';
import { ActionType } from '../actions/DarkModeAction';
import {describe, expect, it} from "@jest/globals";

describe('Theme Reducer', () => {
    // it('should return the initial state', () => {
    //     expect(themeReducer(undefined, {})).toEqual({
    //         theme: 'dark'
    //     });
    // });

    it('should handle SET_THEME', () => {
        const action = {
            type: ActionType.SET_THEME,
            payload: 'light'
        };

        expect(themeReducer(undefined, action)).toEqual({
            theme: 'light'
        });
    });
});
