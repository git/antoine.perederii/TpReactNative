import {CustomJoke} from "../../model/CustomJoke";
import {SampleJoke} from "../../model/SampleJoke";
import {Action, ActionType} from "../actions/CategoryAction";
import {Category} from "../../model/Category";

interface State {
    categories: Category[];
}

const initialState = {
    categories: []
}

const categoryReducer = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case ActionType.FETCH_CATEGORIES:
            // @ts-ignore
            return {...state, categories: action.payload};
        default:
            return state;
    }
}

export default categoryReducer;