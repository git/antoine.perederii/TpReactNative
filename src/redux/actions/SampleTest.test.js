import { ActionType, Action, setJokesList, setJokeById } from './SampleAction';
import { SampleJoke } from '../../model/SampleJoke';
import {describe, expect, it} from "@jest/globals";

describe('Actions', () => {
    describe('setJokesList', () => {
        it('should create an action to set jokes list', () => {
            const jokesList: SampleJoke[] = [
                new SampleJoke(1, 'pun', 'Setup 1', 'https://example.com/image1.jpg', 'Punchline 1'),
                new SampleJoke(2, 'pun', 'Setup 2', 'https://example.com/image2.jpg', 'Punchline 2')
            ];
            const expectedAction: Action = {
                type: ActionType.FETCH_JOKES,
                payload: jokesList,
            };
            const action = setJokesList(jokesList);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('setJokeById', () => {
        it('should create an action to set joke by ID', () => {
            const joke: SampleJoke = new SampleJoke(1, 'pun', 'Setup', 'https://example.com/image.jpg', 'Punchline');
            const expectedAction: Action = {
                type: ActionType.FETCH_JOKES_BY_ID,
                payload: joke,
            };
            const action = setJokeById(joke);
            expect(action).toEqual(expectedAction);
        });
    });
});
