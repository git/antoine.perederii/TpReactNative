import { ActionType, Action, setPostJoke, setCustomJokesList, setCustomJokeById, setDeleteJoke } from './CustomJoke';
import { CustomJoke } from '../../model/CustomJoke';
import {describe, expect, it} from "@jest/globals";

describe('Actions', () => {
    describe('setPostJoke', () => {
        it('should create an action to post a custom joke', () => {
            const customJoke: CustomJoke = new CustomJoke('1', 'pun', 'Setup', 'https://example.com/image.jpg', 'Punchline');
            const expectedAction: Action = {
                type: ActionType.POST_CUSTOM_JOKE,
                payload: customJoke,
            };
            const action = setPostJoke(customJoke);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('setCustomJokesList', () => {
        it('should create an action to set custom jokes list', () => {
            const customJokesList: CustomJoke[] = [
                new CustomJoke('1', 'pun', 'Setup 1', 'https://example.com/image1.jpg', 'Punchline 1'),
                new CustomJoke('2', 'pun', 'Setup 2', 'https://example.com/image2.jpg', 'Punchline 2')
            ];
            const expectedAction: Action = {
                type: ActionType.FETCH_CUSTOM_JOKES,
                payload: customJokesList,
            };
            const action = setCustomJokesList(customJokesList);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('setCustomJokeById', () => {
        it('should create an action to set custom joke by ID', () => {
            const customJoke: CustomJoke = new CustomJoke('1', 'pun', 'Setup', 'https://example.com/image.jpg', 'Punchline');
            const expectedAction: Action = {
                type: ActionType.FETCH_JOKES_BY_ID,
                payload: customJoke,
            };
            const action = setCustomJokeById(customJoke);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('setDeleteJoke', () => {
        it('should create an action to delete a custom joke', () => {
            const jokeId = '1';
            const expectedAction: Action = {
                type: ActionType.DELETE_CUSTOM_JOKE,
                payload: jokeId,
            };
            const action = setDeleteJoke(jokeId);
            expect(action).toEqual(expectedAction);
        });
    });
});
