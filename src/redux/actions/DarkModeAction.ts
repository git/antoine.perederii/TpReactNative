
export enum ActionType {
    SET_THEME = 'SET_THEME',
}

type actionFetch = {
    type: ActionType.SET_THEME;
    payload: String;
}

export type Action = actionFetch;

export const setTheme = (theme) => {
    return {
        type: ActionType.SET_THEME,
        payload: theme,
    };
}