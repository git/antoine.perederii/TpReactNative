import {CustomJoke} from "../../model/CustomJoke";
import {SampleJoke} from "../../model/SampleJoke";

export enum ActionType {
    FETCH_CUSTOM_JOKES = 'FETCH_CUSTOM_JOKES',
    POST_CUSTOM_JOKE = "POST_CUSTOM_JOKE",
    FETCH_JOKES_BY_ID = "FETCH_JOKES_BY_ID",
    DELETE_CUSTOM_JOKE = "DELETE_CUSTOM_JOKE"
}

type actionPostFetch = {
    type: ActionType.POST_CUSTOM_JOKE;
    payload: CustomJoke;
}
type actionGetFetch = {
    type: ActionType.FETCH_CUSTOM_JOKES;
    payload: CustomJoke[];
}
type actionGetByFetch = {
    type: ActionType.FETCH_JOKES_BY_ID;
    payload: CustomJoke;
}
type actionDeleteFetch = {
    type: ActionType.DELETE_CUSTOM_JOKE;
    payload: string;
}

export type Action = actionPostFetch | actionGetFetch | actionGetByFetch | actionDeleteFetch;

export const setPostJoke = (customJoke: CustomJoke) => {
    return {
        type: ActionType.POST_CUSTOM_JOKE,
        payload: customJoke
    }
}
export const setCustomJokesList = (customJokesList: CustomJoke[]) => {
    return {
        type: ActionType.FETCH_CUSTOM_JOKES,
        payload: customJokesList
    };
}
export const setCustomJokeById = (customJoke: CustomJoke) => {
    return {
        type: ActionType.FETCH_JOKES_BY_ID,
        payload: customJoke,
    };
}
export const setDeleteJoke = (jokeId: string) => {
    return {
        type: ActionType.DELETE_CUSTOM_JOKE,
        payload: jokeId
    }
}