import {CustomJoke} from "../../model/CustomJoke";
import {SampleJoke} from "../../model/SampleJoke";

export enum ActionType {
    ADD_FAVORITE = 'ADD_CUSTOM_FAVORITE',
    REMOVE_FAVORITE = "REMOVE_CUSTOM_FAVORITE"
}

type actionAddFetch = {
    type: ActionType.ADD_FAVORITE;
    payload: string;
}
type actionRemoveFetch = {
    type: ActionType.REMOVE_FAVORITE;
    payload: string;
}

export type Action =  actionAddFetch | actionRemoveFetch;

export const addFavorite = (joke: CustomJoke | SampleJoke) => ({
    type: ActionType.ADD_FAVORITE,
    payload: joke,
});

export const removeFavorite = (joke: CustomJoke | SampleJoke) => ({
    type: ActionType.REMOVE_FAVORITE,
    payload: joke,
});