import {SampleJoke} from "../../model/SampleJoke";

export enum ActionType {
  FETCH_JOKES = 'FETCH_JOKES',
  FETCH_JOKES_BY_ID = 'FETCH_JOKES_BY_ID',
}

type actionFetch = {
  type: ActionType.FETCH_JOKES;
  payload: SampleJoke[];
}
type actionFetchById = {
  type: ActionType.FETCH_JOKES_BY_ID;
  payload: SampleJoke;
}
export type Action = actionFetch | actionFetchById;

export const setJokesList = (jokesList: SampleJoke[]) => {
  return {
    type: ActionType.FETCH_JOKES,
    payload: jokesList,
  };
}

export const setJokeById = (joke: SampleJoke) => {
  return {
    type: ActionType.FETCH_JOKES_BY_ID,
    payload: joke,
  };
}