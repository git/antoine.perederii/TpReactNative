import { ActionType, Action, setCategoriesList } from './CategoryAction';
import { Category } from '../../model/Category';
import {describe, expect, it} from "@jest/globals";

describe('Actions', () => {
    describe('setCategoriesList', () => {
        it('should create an action to set categories list', () => {
            const categoriesList: Category[] = [
                new Category('Category 1', 1),
                new Category('Category 2', 2)
            ];

            const expectedAction: Action = {
                type: ActionType.FETCH_CATEGORIES,
                payload: categoriesList,
            };

            const action = setCategoriesList(categoriesList);

            expect(action).toEqual(expectedAction);
        });
    });
});