import { setDeleteJoke } from "../actions/CustomJoke";

export const deleteItem = (id: string) => {
    return async dispatch => {
        try {
            const response = await fetch(`https://iut-weather-api.azurewebsites.net/jokes/${id}`, {
                method: 'DELETE',
                headers: {
                    Accept: "application/json",
                    "Content-Type": 'application/json',
                }
            });

            if (response.ok) {
                dispatch(setDeleteJoke(id)); // Supprimer la blague dans le store Redux
                console.log('Suppression de la blague réussie');
            } else {
                console.log('Erreur lors de la requête DELETE');
            }
        } catch (error) {
            console.log('Erreur :', error);
        }
    };
};

export const deleteCustomJoke = (jokeId) => {
    return deleteItem(jokeId)
}