import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import { deleteItem } from './DeleteThunk'; // Assurez-vous d'importer correctement votre thunk
import { setDeleteJoke } from '../actions/CustomJoke';
import {afterEach, describe, expect, it, jest} from "@jest/globals";

const middlewares = [thunk];
// @ts-ignore
const mockStore = configureMockStore(middlewares);

describe('deleteItem Thunk', () => {
    afterEach(() => {
        fetchMock.restore();
    });

    it('dispatches setDeleteJoke after successful DELETE request', async () => {
        const jokeId = '123';
        const expectedActions = [setDeleteJoke(jokeId)];

        fetchMock.deleteOnce(`https://iut-weather-api.azurewebsites.net/jokes/${jokeId}`, {
            status: 200,
        });

        const store = mockStore({});

        // @ts-ignore
        await store.dispatch(deleteItem(jokeId));

        expect(store.getActions()).toEqual(expectedActions);
    });

    it('logs an error message if DELETE request fails', async () => {
        const jokeId = '123';

        fetchMock.deleteOnce(`https://iut-weather-api.azurewebsites.net/jokes/${jokeId}`, {
            status: 500,
        });

        const consoleSpy = jest.spyOn(console, 'log');
        consoleSpy.mockImplementation(() => {});

        const store = mockStore({});

        // @ts-ignore
        await store.dispatch(deleteItem(jokeId));

        expect(consoleSpy).toHaveBeenCalledWith('Erreur lors de la requête DELETE');

        consoleSpy.mockRestore();
    });
});
