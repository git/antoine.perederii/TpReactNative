// Fonction pour ajouter une blague aux favoris
import AsyncStorage from "@react-native-async-storage/async-storage";
import {SampleJoke} from "../../model/SampleJoke";
import {CustomJoke} from "../../model/CustomJoke";

const addFavorite = async (joke: SampleJoke | CustomJoke) => {
    try {
        let favorites: { sampleJokes: SampleJoke[], customJokes: CustomJoke[] } = await AsyncStorage.getItem('@favorites');
        if (!favorites) {
            favorites = { sampleJokes: [], customJokes: [] };
        }
        if (joke instanceof SampleJoke) {
            favorites.sampleJokes.push(joke);
        } else if (joke instanceof CustomJoke) {
            favorites.customJokes.push(joke);
        }
        await AsyncStorage.setItem('@favorites', JSON.stringify(favorites));
    } catch (error) {
        console.error('Error adding favorite:', error);
    }
};

const removeFavorite = async (jokeId: string | number) => {
    try {
        var favorites: { sampleJokes: SampleJoke[], customJokes: CustomJoke[] } = await AsyncStorage.getItem('@favorites');
        if (!favorites) {
            return;
        }
        favorites.sampleJokes = favorites.sampleJokes.filter(joke => joke.id !== jokeId);
        favorites.customJokes = favorites.customJokes.filter(joke => joke.id !== jokeId);
        await AsyncStorage.setItem('@favorites', JSON.stringify(favorites));
    } catch (error) {
        console.error('Error removing favorite:', error);
    }
};

const getFavorites = async () => {
    try {
        const favoritesString = await AsyncStorage.getItem('@favorites');
        if (favoritesString !== null) {
            return JSON.parse(favoritesString);
        }
    } catch (error) {
        console.error('Error getting favorites:', error);
    }
    return { sampleJokes: [], customJokes: [] };
};
