import {SampleJoke} from "../../model/SampleJoke";
import {setJokesList} from "../actions/SampleAction";
import {Category} from "../../model/Category";
import {setCategoriesList} from "../actions/CategoryAction";
import {CustomJoke} from "../../model/CustomJoke";
import {setCustomJokesList} from "../actions/CustomJoke";

export const getList = <TList>(uri:string, constructor : (json:any) => TList, setList: (list: TList[]) => any) => {
    return async dispatch => {
        try {
            const promise = await fetch(uri);
            const listJson = await promise.json();
            const List: TList[] = listJson.map(elt => constructor(elt));
            dispatch(setList(List));
        } catch (error) {
            console.log(`Error In getList ${uri}  ---------`, error);
        }
    }
}

export const getLastSampleJokesList = () => {
    return getList('https://iut-weather-api.azurewebsites.net/jokes/lasts', (elt) => new SampleJoke(elt["id"], elt["type"], elt["setup"], elt["image"]), (list) => setJokesList(list))
}

export const getCategoriesList = () => {
    return getList('https://iut-weather-api.azurewebsites.net/jokes/categories/top', (elt) => new Category(elt["name"], elt["number"]), (list) => setCategoriesList(list))
}

export const getSampleJokesList = () => {
    return getList('https://iut-weather-api.azurewebsites.net/jokes/samples', (elt) => new SampleJoke(elt["id"], elt["type"], elt["setup"], elt["image"]), (list) => setJokesList(list))
}

export const getCustomJokesList = () => {
    return getList('https://iut-weather-api.azurewebsites.net/jokes', (elt) => new CustomJoke(elt["id"], elt["type"], elt["setup"], elt["image"]), (list) => setCustomJokesList(list))
}