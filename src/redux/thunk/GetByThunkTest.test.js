import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import { getItem, getJokeById, getCustomJokeById } from './GetByThunk'; // Assurez-vous d'importer correctement vos thunks
import { setJokeById } from '../actions/SampleAction';
import {setCustomJokeById} from "../actions/CustomJoke";
import { SampleJoke } from '../../model/SampleJoke';
import { CustomJoke } from '../../model/CustomJoke';
import {afterEach, describe, expect, it, jest} from "@jest/globals";

const middlewares = [thunk];
// @ts-ignore
const mockStore = configureMockStore(middlewares);

describe('getItem Thunk', () => {
    afterEach(() => {
        fetchMock.restore();
    });

    it('dispatches setJokeById after successful GET request for SampleJoke', async () => {
        const jokeId = 1;
        const expectedJoke = new SampleJoke(jokeId, 'type', 'setup', 'image');

        fetchMock.getOnce(`https://iut-weather-api.azurewebsites.net/jokes/samples/${jokeId}`, {
            status: 200,
            body: {
                id: jokeId,
                type: 'type',
                setup: 'setup',
                image: 'image',
            },
        });

        const store = mockStore({});

        // @ts-ignore
        await store.dispatch(getJokeById(parseInt(jokeId)));

        expect(store.getActions()).toEqual([setJokeById(expectedJoke)]);
    });

    it('dispatches setCustomJokeById after successful GET request for CustomJoke', async () => {
        const jokeId = '1';
        const expectedJoke = new CustomJoke(jokeId, 'type', 'setup', 'image');

        fetchMock.getOnce(`https://iut-weather-api.azurewebsites.net/jokes/${jokeId}`, {
            status: 200,
            body: {
                id: jokeId,
                type: 'type',
                setup: 'setup',
                image: 'image',
            },
        });

        const store = mockStore({});

        // @ts-ignore
        await store.dispatch(getCustomJokeById(jokeId));

        expect(store.getActions()).toEqual([setCustomJokeById(expectedJoke)]);
    });

    it('logs an error message if GET request fails', async () => {
        const jokeId = '1';

        fetchMock.getOnce(`https://iut-weather-api.azurewebsites.net/jokes/samples/${jokeId}`, {
            status: 500,
        });

        const consoleSpy = jest.spyOn(console, 'log');
        consoleSpy.mockImplementation(() => {});

        const store = mockStore({});

        // @ts-ignore
        await store.dispatch(getJokeById(parseInt(jokeId)));

        expect(consoleSpy).toHaveBeenCalledWith('Error---------');
    });
});
