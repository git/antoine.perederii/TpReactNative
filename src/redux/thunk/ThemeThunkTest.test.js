import AsyncStorage from "@react-native-async-storage/async-storage";
import { storeTheme, getTheme } from "./ThemeThunk";
import {afterEach, describe, expect, it, jest} from "@jest/globals";

describe('storeTheme Function', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('stores theme in AsyncStorage', async () => {
        const theme = { colors: { background: 'white', text: 'black' } };
        const jsonValue = JSON.stringify(theme);
        AsyncStorage.setItem = jest.fn().mockResolvedValueOnce();
        console.log = jest.fn();

        await storeTheme(theme);
        expect(AsyncStorage.setItem).toHaveBeenCalledWith('@theme', jsonValue);
        expect(console.log).toHaveBeenCalledWith("theme stored");
    });

    it('logs error if theme storage fails', async () => {
        const theme = { colors: { background: 'white', text: 'black' } };
        AsyncStorage.setItem = jest.fn().mockRejectedValueOnce('Storage error');
        console.error = jest.fn();

        await storeTheme(theme);
        expect(console.error).toHaveBeenCalledWith('Storage error');
    });
});

describe('getTheme Function', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('retrieves theme from AsyncStorage', async () => {
        const theme = { colors: { background: 'white', text: 'black' } };
        const jsonValue = JSON.stringify(theme);
        AsyncStorage.getItem = jest.fn().mockResolvedValueOnce(jsonValue);

        const result = await getTheme();
        expect(result).toEqual(theme);
    });

    it('returns null if theme is not found in AsyncStorage', async () => {
        AsyncStorage.getItem = jest.fn().mockResolvedValueOnce(null);

        const result = await getTheme();
        expect(result).toBeNull();
    });

    it('logs error if theme retrieval fails', async () => {
        AsyncStorage.getItem = jest.fn().mockRejectedValueOnce('Retrieval error');
        console.error = jest.fn();

        await getTheme();
        expect(console.error).toHaveBeenCalledWith('Retrieval error');
    });
});
