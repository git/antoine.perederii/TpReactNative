import {SampleJoke} from "../../model/SampleJoke";
import { setJokeById} from "../actions/SampleAction";
import {CustomJoke} from "../../model/CustomJoke";
import {setCustomJokeById} from "../actions/CustomJoke";

export const getItem = <TItem>(uri:string, constructor : (json:any) => TItem, setItem: (item: TItem) => any) => {
    return async dispatch => {
        try {
            console.log(";;;;;;;;;;;;;;", uri, ";;;;;;;;;;;;;;;;;;;")
            const promise = await fetch(uri);
            const Json = await promise.json();
            const Item: TItem = constructor(Json);
            console.log("===========", Item , "===================");
            dispatch(setItem(Item));
        } catch (error) {
            console.log('Error---------', error);
        }
    }
}
export const getJokeById = (id : number) => {
    return getItem<SampleJoke>('https://iut-weather-api.azurewebsites.net/jokes/samples/' + id.toString() , (elt) => new SampleJoke(elt["id"], elt["type"], elt["setup"], elt["image"]), (item) => setJokeById(item))
}

export const getCustomJokeById = (id: string) => {
    return getItem<CustomJoke>('https://iut-weather-api.azurewebsites.net/jokes/' + id , (elt) => new CustomJoke(elt["id"], elt["type"], elt["setup"], elt["image"]), (item) => setCustomJokeById(item))
}