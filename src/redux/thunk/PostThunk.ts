import {setPostJoke} from "../actions/CustomJoke";

export const setItem = <TItem>(
    uri: string,
    type : string,
    setup : string,
    punchline : string
) => {
    return async dispatch => {
        try {
            // @ts-ignore
            const response = await fetch(uri, {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    "Content-Type": 'application/json',
                },
                body: JSON.stringify(
                    {
                        type: type,
                        setup: setup,
                        punchline: punchline
                    }
                )
            });
            const data = await response.json();
            dispatch(setPostJoke(data));
            if (response.ok) {
                console.log('Envoie ok de custom joke')
            } else {
                console.log('Erreur lors de la requête POST');
            }
        } catch (error) {
            console.log('Erreur :', error);
        }
    };
};

export const postCustomJoke = (joke, downgrade, category) => {
    return setItem('https://iut-weather-api.azurewebsites.net/jokes', joke, downgrade, category)
}