import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import { setPostJoke } from '../actions/CustomJoke';
import { setItem, postCustomJoke } from './PostThunk';
import {afterEach, describe, expect, it, jest} from "@jest/globals";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('setItem Thunk', () => {
    afterEach(() => {
        fetchMock.restore();
    });

    it('dispatches setPostJoke after successful POST request', async () => {
        const mockResponse = { id: '123', type: 'pun', setup: 'Why was the math book sad?', punchline: 'Because it had too many problems.' };
        fetchMock.postOnce('https://iut-weather-api.azurewebsites.net/jokes', {
            body: mockResponse,
            headers: { 'content-type': 'application/json' }
        });

        const expectedActions = [
            setPostJoke(mockResponse)
        ];
        const store = mockStore({});

        await store.dispatch(setItem('https://iut-weather-api.azurewebsites.net/jokes', 'pun', 'Why was the math book sad?', 'Because it had too many problems.'));
        expect(store.getActions()).toEqual(expectedActions);
    });

    it('logs an error message if POST request fails', async () => {
        fetchMock.postOnce('https://iut-weather-api.azurewebsites.net/jokes', 404);

        const consoleSpy = jest.spyOn(console, 'log');
        consoleSpy.mockImplementation(() => {});

        const store = mockStore({});

        await store.dispatch(setItem('https://iut-weather-api.azurewebsites.net/jokes', 'pun', 'Why was the math book sad?', 'Because it had too many problems.'));
        expect(consoleSpy).toHaveBeenCalledWith('Erreur lors de la requête POST');

        consoleSpy.mockRestore();
    });
});

describe('postCustomJoke Thunk', () => {
    afterEach(() => {
        fetchMock.restore();
    });

    it('calls setItem with correct parameters', async () => {
        const uri = 'https://iut-weather-api.azurewebsites.net/jokes';
        const type = 'pun';
        const setup = 'Why was the math book sad?';
        const punchline = 'Because it had too many problems.';
        const store = mockStore({});

        const setItemSpy = jest.spyOn(global, 'setItem');
        setItemSpy.mockResolvedValueOnce();

        await store.dispatch(postCustomJoke(type, setup, punchline));
        expect(setItemSpy).toHaveBeenCalledWith(uri, type, setup, punchline);

        setItemSpy.mockRestore();
    });
});
