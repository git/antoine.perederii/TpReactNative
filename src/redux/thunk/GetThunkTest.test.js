import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import { getLastSampleJokesList, getCategoriesList } from './GetThunk';
import { setCategoriesList } from '../actions/CategoryAction';

import { SampleJoke } from '../../model/SampleJoke';
import { Category } from '../../model/Category';
import {afterEach, describe, expect, it, jest} from "@jest/globals";
import {setJokesList} from "../actions/SampleAction";
import thunk from "redux-thunk";

const middlewares = [thunk];
// @ts-ignore
const mockStore = configureMockStore(middlewares);

describe('getList Thunk', () => {
    afterEach(() => {
        fetchMock.restore();
    });

    it('dispatches setJokesList after successful GET request for LastSampleJokesList', async () => {
        const expectedJokesList = [
            new SampleJoke('1', 'type1', 'setup1', 'image1'),
            new SampleJoke('2', 'type2', 'setup2', 'image2'),
        ];

        fetchMock.getOnce('https://iut-weather-api.azurewebsites.net/jokes/lasts', {
            status: 200,
            body: expectedJokesList.map(joke => ({
                id: joke.id,
                type: joke.type,
                setup: joke.setup,
                image: joke.image,
            })),
        });

        const store = mockStore({});

        // @ts-ignore
        await store.dispatch(getLastSampleJokesList());

        expect(store.getActions()).toEqual([setJokesList(expectedJokesList)]);
    });

    it('dispatches setCategoriesList after successful GET request for CategoriesList', async () => {
        const expectedCategoriesList = [
            new Category('Category1', 1),
            new Category('Category2', 2),
        ];

        fetchMock.getOnce('https://iut-weather-api.azurewebsites.net/jokes/categories/top', {
            status: 200,
            body: expectedCategoriesList.map(category => ({
                name: category.name,
                number: category.number,
            })),
        });

        const store = mockStore({});

        // @ts-ignore
        await store.dispatch(getCategoriesList());

        expect(store.getActions()).toEqual([setCategoriesList(expectedCategoriesList)]);
    });

    // Tests similaires pour les autres fonctions thunks

    it('logs an error message if GET request fails', async () => {
        fetchMock.getOnce('https://iut-weather-api.azurewebsites.net/jokes/lasts', {
            status: 500,
        });

        const consoleSpy = jest.spyOn(console, 'log');
        consoleSpy.mockImplementation(() => {});

        const store = mockStore({});

        // @ts-ignore
        await store.dispatch(getLastSampleJokesList());

        expect(consoleSpy).toHaveBeenCalledWith(expect.stringContaining('Error In getList'));
    });
});
