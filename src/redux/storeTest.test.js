import store from './store';
import {describe, expect, it} from "@jest/globals";

describe('Redux Store Configuration', () => {
    it('should have sampleReducer', () => {
        expect(store.getState().sampleReducer).toBeDefined();
    });

    it('should have categoryReducer', () => {
        expect(store.getState().categoryReducer).toBeDefined();
    });

    it('should have customReducer', () => {
        expect(store.getState().customReducer).toBeDefined();
    });

    it('should have themeReducer', () => {
        expect(store.getState().theme).toBeDefined();
    });

    it('should have favoriteReducer', () => {
        expect(store.getState().favorite).toBeDefined();
    });

    it('should have a configured store', () => {
        expect(store).toBeDefined();
        expect(store.getState()).toBeDefined();
    });
});
