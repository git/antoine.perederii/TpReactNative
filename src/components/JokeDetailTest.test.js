import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import JokeDetail from '../path/to/JokeDetail';
import {describe} from "@jest/globals"; // Remplacez le chemin par le bon chemin

describe('JokeDetail Component', () => {
    const sampleJoke = {
        id: '1',
        type: 'Sample',
        image: 'sample-image-url',
        description: () => 'Sample Joke Description'
    };

    const customJoke = {
        id: '2',
        type: 'Custom',
        image: 'custom-image-url',
        description: () => 'Custom Joke Description'
    };

    test('renders joke image', () => {
        const { getByTestId } = render(<JokeDetail joke={sampleJoke} />);
        const jokeImage = getByTestId('joke-image');
        expect(jokeImage).toBeTruthy();
    });

    test('renders joke description when toggled', () => {
        const { getByText } = render(<JokeDetail joke={sampleJoke} />);
        const toggleButton = getByText('LA CHUTE');
        fireEvent.press(toggleButton);
        const jokeDescription = getByText('Sample Joke Description');
        expect(jokeDescription).toBeTruthy();
    });

    test('toggles favorite icon when pressed', () => {
        const { getByTestId } = render(<JokeDetail joke={sampleJoke} />);
        const favoriteButton = getByTestId('favorite-button');
        fireEvent.press(favoriteButton);
        expect(favoriteButton.props.source).toEqual(require('../assets/plain_favorite_icon.png'));
    });

    test('calls deleteCustomJokes and updates list when delete button is pressed for custom joke', () => {
        const deleteMock = jest.fn();
        const dispatchMock = jest.fn();
        jest.mock('../redux/thunk/DeleteThunk', () => ({ deleteCustomJoke: deleteMock }));
        jest.mock('react-redux', () => ({ useDispatch: () => dispatchMock }));

        const { getByTestId } = render(<JokeDetail joke={customJoke} />);
        const deleteButton = getByTestId('delete-button');
        fireEvent.press(deleteButton);
        expect(deleteMock).toHaveBeenCalledWith(customJoke.id);
        expect(dispatchMock).toHaveBeenCalledTimes(2); // Assuming one dispatch for delete and one for update list
    });
});