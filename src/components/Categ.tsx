import {StyleSheet, Text, View} from 'react-native';
import {greyColor} from "../assets/Theme";
import {Category} from "../model/Category";

type CategItemProps = {
    category: Category;
};

export default function Categ(prop: CategItemProps) {
    return (
        <View style={styles.bottomContainer}>
            <Text style={{color:'white'}}>{prop.category.name}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    bottomContainer: {
        backgroundColor: greyColor,
        paddingVertical: 5,
        paddingHorizontal: 10,
        margin: 10,
        borderRadius: 20,
        width : 120,
        alignItems : 'center'
    }
});