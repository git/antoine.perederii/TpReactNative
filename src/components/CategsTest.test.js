import React from 'react';
import { shallow } from 'enzyme';
import Categs from './Categs';
import { FlatList } from 'react-native';
import Categ from './Categ';
import { Category } from '../model/Category';
import {describe, expect, it} from "@jest/globals";

describe('Categs Component', () => {
    const categories: Category[] = [
        new Category('Category 1', 10),
        new Category('Category 2', 5),
        new Category('Category 3', 15),
    ];

    it('renders correctly', () => {
        const wrapper = shallow(<Categs categories={categories} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('renders FlatList with correct props', () => {
        const wrapper = shallow(<Categs categories={categories} />);
        const flatList = wrapper.find(FlatList);
        expect(flatList.exists()).toBeTruthy();
        expect(flatList.prop('showsHorizontalScrollIndicator')).toBe(false);
        expect(flatList.prop('horizontal')).toBe(true);
        expect(flatList.prop('data')).toEqual(categories.sort((a, b) => b.number - a.number));
        expect(flatList.prop('keyExtractor')).toBeInstanceOf(Function);
        expect(flatList.prop('renderItem')).toBeInstanceOf(Function);
    });

    it('renders correct number of Categ components', () => {
        const wrapper = shallow(<Categs categories={categories} />);
        expect(wrapper.find(Categ)).toHaveLength(categories.length);
    });
});
