import {StyleSheet, Text, View, Image, Button, TouchableOpacity} from 'react-native';
import {SampleJoke} from "../model/SampleJoke";
import {darksalmonColor, whiteColor, greyColor, indigoColor, purpleColor} from "../assets/Theme";
import React, {useEffect, useState} from "react";
import {CustomJoke} from "../model/CustomJoke";
import {useDispatch, useSelector} from "react-redux";
import {getCustomJokeById, getJokeById} from "../redux/thunk/GetByThunk";
import {getCustomJokesList} from "../redux/thunk/GetThunk";
import {deleteCustomJoke} from "../redux/thunk/DeleteThunk";
import {useNavigation} from "@react-navigation/native";
import {AppDispatch} from "../redux/store";

type JokeItemProps = {
    joke: SampleJoke | CustomJoke;
};
const eye = require("../assets/eye_icon.png")
const hideEye = require("../assets/eye_off_icon.png")
const heart = require("../assets/favorite_icon.png")
const bin = require("../assets/bin.png")
const heartPlain = require("../assets/plain_favorite_icon.png")
export default function JokeDetail(props: JokeItemProps) {
    const [showDescription, setShowDescription] = useState(false);
    const [showFavorite, setShowFavorite] = useState(false);
    const isCustom = props.joke instanceof CustomJoke;
    const dispatch = useDispatch<AppDispatch>();
    const navigation = useNavigation();

    const toggleDescription = () => {
        setShowDescription(!showDescription);
    };

    const toggleFavorite = () => {
        setShowFavorite(!showFavorite);
    };

    const deleteCustomJokes = async () => {
        await dispatch(deleteCustomJoke(props.joke.id));
        await dispatch(getCustomJokesList());
        navigation.goBack();
    };

    return(
        <View style={styles.container}>
            <Image source={{ uri: props.joke.image }} style={styles.image} />
            <View style={styles.bottomContainer}>
                <Text style={{color: indigoColor}}>{props.joke.type}</Text>
            </View>
            <Text style={styles.text}>Résumé de la blague</Text>
            <View style={styles.row}>
                {isCustom && (<TouchableOpacity style={styles.favContainer} onPress={ deleteCustomJokes }>
                                <View>
                                    <Image
                                        source={bin}
                                        style={styles.imageButton}
                                    />
                                </View>
                            </TouchableOpacity>)
                }
                <TouchableOpacity style={styles.favContainer} onPress={toggleFavorite}>
                    <View>
                        <Image
                            source={showFavorite ? heartPlain : heart}
                            style={styles.imageButton}
                        />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.Button} onPress={toggleDescription}>
                    <View style={styles.chuteContainer}>
                        <Image
                            source={showDescription ? hideEye : eye}
                            style={styles.imageButton}
                        />
                        <Text style={styles.TextButton} >LA CHUTE</Text>
                    </View>
                </TouchableOpacity>
            </View>
            {showDescription && <Text style={styles.text}>{props.joke.description()}</Text>}
        </View>
    );
}

const styles = StyleSheet.create({
    image : {
        margin : 5,
        width: '90%',
        height:200,
        top : 5,
        alignSelf : "center",
        backgroundColor: "white",
        borderRadius: 5,
    },
    Button:{
        borderRadius : 5,
        backgroundColor : darksalmonColor,
        height:50,
        width : 160,
        flexDirection : "row"
    },
    imageButton : {
        margin : 10,
        width: 50,
        height:50,
        top : 5,
        alignSelf : "center",
        backgroundColor: "none",
        tintColor : whiteColor
    },
    favContainer : {
        margin : 20,
        borderWidth : 3,
        borderRadius : 15,
        borderColor : whiteColor,
        borderStyle : "solid"
    },
    TextButton : {
        margin: 10,
        textAlign : "center",
        fontWeight: "700",
        color : whiteColor,
    },
    chuteContainer :{
        display : "flex",
        flex : 1,
        flexDirection: "row",
        alignItems : "center"
    },
    container: {
        marginHorizontal: "5%",
        display: "flex",
        marginBottom:7,
        marginTop:7,
        paddingVertical: 10,
        backgroundColor: indigoColor,
        justifyContent: 'space-between',
        borderRadius: 20,
        height: "auto",
        borderColor: whiteColor,
        borderStyle: "solid",
        borderWidth: 1
    },
    row: {
        display: "flex",
        flexDirection:"row",
        alignSelf: "flex-end",
    },
    color: {
        flex: 0,
        backgroundColor: darksalmonColor,
        height: 150,
        width:15,
    },
    columnContainer: {
        flexDirection: "column",
        marginLeft: 20,
        marginRight: 20,
        width: '60%',
        flex: 2,
        justifyContent: 'space-between',
    },
    text: {
        color:greyColor,
        paddingBottom: 7,
        paddingTop: 7,
        marginLeft: 19,
        fontSize: 16,
    },
    bottomContainer: {
        backgroundColor: whiteColor,
        paddingVertical: 5,
        paddingHorizontal: 10,
        margin: 10,
        marginLeft: 19,
        marginTop: 20,
        borderRadius: 20,
        width : 150,
        alignItems : 'center'
    }
})