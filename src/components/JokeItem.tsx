import {StyleSheet, Text, View, Image} from 'react-native';
import {SampleJoke} from "../model/SampleJoke";
import {CustomJoke} from "../model/CustomJoke";
import {darksalmonColor, whiteColor, greyColor, indigoColor} from "../assets/Theme";
import Categ from "./Categ";

type JokeListItemProps = {
    joke: (CustomJoke | SampleJoke);
};

export default function JokeItem(prop: JokeListItemProps) {
    return (
        <View style={styles.rowContainer}>
            <View style={styles.color}/>
            <Image source={{ uri: prop.joke.image }} style={styles.image} />
            <View style={styles.columnContainer}>
                <Text style={styles.text}>Résumé de la blague</Text>
                <Text style={styles.text}>{prop.joke.description()}</Text>
                <View style={styles.bottomContainer}>
                    <Text style={{color:'white'}}>{prop.joke.type}</Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    rowContainer: {
        flexDirection: "row",
        marginHorizontal: "5%",
        marginBottom:7,
        marginTop:7,
        paddingVertical: 10,
        backgroundColor: indigoColor,
        width:'90%',
        height:150,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    color: {
        flex: 0,
        backgroundColor: darksalmonColor,
        height: 150,
        width:15,
    },
    image: {
        width: '40%',
        height: 150,
        flex: 1
    },
    columnContainer: {
        flexDirection: "column",
        marginLeft: 20,
        marginRight: 20,
        width: '60%',
        flex: 2,
        justifyContent: 'space-between',
    },
    text: {
        color:whiteColor,
        paddingBottom: 7,
        paddingTop: 7,
        fontSize: 16,
    },
    bottomContainer: {
        backgroundColor: greyColor,
        paddingVertical: 5,
        paddingHorizontal: 10,
        margin: 10,
        borderRadius: 20,
        width : 120,
        alignItems : 'center'
    }
});