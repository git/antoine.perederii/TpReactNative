import {StyleSheet, Text, View, Image} from 'react-native';
import {SampleJoke} from "../model/SampleJoke";
import {CustomJoke} from "../model/CustomJoke";
import {darksalmonColor, whiteColor, indigoColor} from "../assets/Theme";

type JokeListItemProps = {
    joke: (CustomJoke | SampleJoke);
};

export default function JokeHomeSquare(prop: JokeListItemProps) {
    return (
        <View style={styles.container}>
            <View style={styles.topBackgroundColor}>
                <View style={{width: 200, height: 40, backgroundColor: darksalmonColor}}/>
            </View>
            <View style={styles.bottomBackgroundColor}>
                <View style={{width: 200, height: 120, backgroundColor: indigoColor}}/>
            </View>
            <Image source={{ uri: prop.joke.image }} style={styles.image} />
            <Text style={[styles.text, styles.textTitle]}>Résumé de la blague</Text>
            <Text style={[styles.text, styles.textSimple]}>{prop.joke.description()}</Text>
        </View>
        );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginLeft: 20,
        position: "relative",
    },
    topBackgroundColor: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        overflow: 'hidden',
    },
    bottomBackgroundColor: {
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        overflow: 'hidden',
    },
    image: {
        width: 120,
        height: 75,
        position: "absolute",
        top: 5,
        left: "50%",
        marginLeft: -60
    },
    text: {
        position: 'absolute',
        textAlign: "center",
        color:whiteColor,
        paddingBottom: 7,
        paddingTop: 7,
        width: "100%"
    },
    textTitle: {
        fontSize: 17,
        fontWeight: "bold",
        top: 90
    },
    textSimple: {
        fontSize: 15,
        top: 115
    }
});