import {FlatList} from 'react-native';
import {SampleJoke} from "../model/SampleJoke";
import {CustomJoke} from "../model/CustomJoke";
import JokeHomeSquare from "./JokeHomeSquare";

type JokeListItemProps = {
    jokes: (CustomJoke | SampleJoke)[];
};

export default function JokesHomeSquare(props: JokeListItemProps) {
    return (
        <FlatList showsHorizontalScrollIndicator={false} horizontal={true}
            data={props.jokes}
            renderItem={
                ({ item }: { item: CustomJoke | SampleJoke }) => (
                    <JokeHomeSquare joke={item}/>
                )
            }
            keyExtractor={(item) => item.id.toString()}
        />
    );
}