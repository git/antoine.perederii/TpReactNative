import React from 'react';
import { shallow } from 'enzyme';
import Categ from './Categ';
import { Text, View } from 'react-native';
import {describe, expect, it} from "@jest/globals";

describe('Categ Component', () => {
    it('renders correctly', () => {
        const category = { name: 'Test Category' };
        const wrapper = shallow(<Categ category={category} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('displays category name', () => {
        const category = { name: 'Test Category' };
        const wrapper = shallow(<Categ category={category} />);
        expect(wrapper.find(Text).prop('children')).toEqual(category.name);
    });

    it('applies correct styles', () => {
        const category = { name: 'Test Category' };
        const wrapper = shallow(<Categ category={category} />);
        const containerStyle = wrapper.find(View).prop('style');
        expect(containerStyle).toEqual(expect.objectContaining(styles.bottomContainer));
    });
});
