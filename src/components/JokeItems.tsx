import {FlatList, Image, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import {SampleJoke} from "../model/SampleJoke";
import {CustomJoke} from "../model/CustomJoke";
import JokeItem from "./JokeItem";
import React, {useState} from "react";
import {useNavigation} from "@react-navigation/native";
import {darksalmonColor, greyColor, indigoColor, whiteColor} from "../assets/Theme";

type JokeListItemProps = {
    jokes: (CustomJoke | SampleJoke)[];
};

export default function JokeItems(props: JokeListItemProps) {
    const navigation = useNavigation()

    return (
        <FlatList
            data={props.jokes}
            keyExtractor={(item) => item.id.toString()}
            renderItem={
                ({ item }: { item: CustomJoke | SampleJoke }) => (
                    // @ts-ignore
                    <TouchableHighlight onPress={() => navigation.navigate("JokeDetails", {"idJoke": item.id})}>
                        <JokeItem joke={item}/>
                    </TouchableHighlight>
                )
            }
        />
    );
}


const styles = StyleSheet.create({

})