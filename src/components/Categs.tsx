import {FlatList} from 'react-native';
import Categ from "./Categ";
import {Category} from "../model/Category";

type CategListItemProps = {
    categories: Category[];
};

export default function Categs(props: CategListItemProps) {
    return (
        <FlatList showsHorizontalScrollIndicator={false} horizontal={true}
            data={props.categories.sort((a, b) => b.number - a.number)}
            keyExtractor={(item) => item.name}
            renderItem={
                ({ item }: { item: Category }) => (
                    <Categ category={item}/>
                )
            }
        />
    );
}