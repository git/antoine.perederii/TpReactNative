import {DarkTheme, DefaultTheme, NavigationContainer, Theme} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Image, View} from 'react-native';

import HomeScreen from "../screens/HomeScreen";
import Favorites from "../screens/Favorites";
import Add from "../screens/AddScreen";
import Settings from "../screens/Settings";

import {darksalmonColor, greyColor, indigoColor} from "../assets/Theme";
import StackNavigation from "./StackNavigation";
import {useEffect, useState} from "react";
import {getTheme} from "../redux/thunk/ThemeThunk";


export default function NavigationBar() {
    const BottomTabNavigator = createBottomTabNavigator();

    const homeIcon = require("../assets/home_icon.png");
    const listIcon = require("../assets/list_icon.png");
    const addIcon = require("../assets/add_icon.png");
    const favoriteIcon = require("../assets/favorite_icon.png");
    const settingsIcon = require("../assets/settings_icon.png");

    const [themes, setTheme] = useState<Theme>(DefaultTheme);

    useEffect(() => {
        const fetchTheme = async () => {
            const theme = await getTheme();
            setTheme(theme);
        };

        fetchTheme();
    });

    if (themes == null) {
        return null;
    }

    return (
        <NavigationContainer theme={ themes.dark === false ? DefaultTheme :  DarkTheme}>
            <BottomTabNavigator.Navigator initialRouteName="Accueil"
                                          screenOptions={{
                                              headerStyle: {
                                                  backgroundColor: indigoColor,
                                              },
                                              headerTitleStyle: {
                                                  color:darksalmonColor,
                                                  fontSize:24,
                                                  textAlign: "center",
                                                  paddingBottom:20,
                                              },
                                              headerTitleAlign: 'center',
                                              tabBarShowLabel: false,
                                              tabBarStyle: {
                                                  backgroundColor: indigoColor,
                                              }
                                          }}
            >
                <BottomTabNavigator.Screen name="Accueil" component={HomeScreen}
                                           options={{
                                               tabBarIcon: ({focused}) => (
                                                   <Image
                                                       source={homeIcon}
                                                       style={{
                                                           width: 30, height: 30,
                                                           tintColor: focused ? darksalmonColor : greyColor,
                                                       }}
                                                   />
                                               )
                                           }}/>
                <BottomTabNavigator.Screen name="Catalogue" component={StackNavigation}
                                           options={{
                                               headerShown: false,
                                               tabBarIcon: ({focused}) => (
                                                   <Image
                                                       source={listIcon}
                                                       style={{
                                                           width: 30, height: 30,
                                                           tintColor: focused ? darksalmonColor : greyColor,
                                                       }}
                                                   />
                                               )
                                           }}/>
                <BottomTabNavigator.Screen name="Ajout d'une blague" component={Add}
                                           options={{
                                               tabBarIcon: ({focused}) => (
                                                   <View style={{backgroundColor: greyColor, borderRadius: 5, padding: 10}}>
                                                       <Image
                                                           source={addIcon}
                                                           style={{
                                                               width: 20, height: 20,
                                                               tintColor: focused ? darksalmonColor : "black",
                                                           }}
                                                       />
                                                   </View>
                                               )
                                           }}/>
                <BottomTabNavigator.Screen name="Favoris" component={Favorites}
                                           options={{
                                               tabBarIcon: ({focused}) => (
                                                   <Image
                                                       source={favoriteIcon}
                                                       style={{
                                                           width: 30, height: 30,
                                                           tintColor: focused ? darksalmonColor : greyColor,
                                                       }}
                                                   />
                                               )
                                           }}/>
                <BottomTabNavigator.Screen name="Parametres" component={Settings}
                                           options={{
                                               tabBarIcon: ({focused}) => (
                                                   <Image
                                                       source={settingsIcon}
                                                       style={{
                                                           width: 30, height: 30,
                                                           tintColor: focused ? darksalmonColor : greyColor,
                                                       }}
                                                   />
                                               )
                                           }}/>
            </BottomTabNavigator.Navigator>
        </NavigationContainer>
    )
}