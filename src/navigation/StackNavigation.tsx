import {createStackNavigator} from "@react-navigation/stack";
import Catalogue from "../screens/Catalogue";
import JokeDetailsScreen from "../screens/JokeDetailsScreen";
import {darksalmonColor, indigoColor} from "../assets/Theme";

export default function StackNavigation() {
    const Stack = createStackNavigator();
    return (
            <Stack.Navigator initialRouteName="catalogue" screenOptions={{
                                                                headerStyle: {
                                                                    backgroundColor: indigoColor,
                                                                },
                                                                headerTitleStyle: {
                                                                    marginTop: 10,
                                                                    color:darksalmonColor,
                                                                    fontSize:24,
                                                                    textAlign: "center",
                                                                    paddingBottom:30,
                                                                },
                                                                headerTitleAlign: 'center'
                                                            }}>
                <Stack.Screen name="catalogue" component={Catalogue} />
                <Stack.Screen name="JokeDetails" component={JokeDetailsScreen}/>
            </Stack.Navigator>
    )
}