<div align = center>

  <h1>Projet de TP sur ReactNative</h1>
    
</div>



<div align = center>



---

![React Native](https://img.shields.io/badge/react_native-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![Expo](https://img.shields.io/badge/expo-1C1E24?style=for-the-badge&logo=expo&logoColor=#D04A37)
![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)

</div>  
  
# Table des matières
[Présentation](#présentation) | [Répartition du Git](#répartition-du-git) | [Prérequis](#prérequis-pour-le-projet-entity-framework) | [Getting Started](#premiers-pas) | [Avancés du projet](#avancés-du-projet) | [Fabriqué avec](#fabriqué-avec) | [Contributeurs](#contributeurs) | [Comment contribuer](#comment-contribuer) | [Remerciements](#remerciements)



## Présentation

### Récapitulatif du Projet

Ce repos est une mise en application des tp de React Native. Il a pour but de mettre en pratique les connaissances acquises lors des cours et des tp.

## Répartition du Git

[**Sources**](src/tp-react-native) : **Code de l'application**

---

Le projet de tp utilise un modèle de flux de travail Git pour organiser le développement. Voici une brève explication des principales branches :

- **branche master** : Cette branche contient la dernière version stable du projet. 

- **branche partNUMERO** : La branche partNUMERO contient le code de la partie NUMERO du projet. Cette branche est utilisée pour le développement de la partie NUMERO. Une fois la partie NUMERO terminée, la branche est fusionnée avec la branche master.

## Prérequis pour le projet Entity Framework

* [Visual Studio Code](https://code.visualstudio.com/) - Environnement de développement intégré (IDE) recommandé pour le projet multiplateforme
* [Git](https://git-scm.com/) - Outil de versioning

## Premiers Pas
1. Cloner le dépôt Git : `git clone https://codefirst.iut.uca.fr/git/antoine.perederii/TpReactNative.git`
2. Placez-vous dans le dossier du projet : `cd src/TpReactNative`
3. Lancez la commande `npm install` pour installer les dépendances.
4. Lancez la commande `npm start` pour lancer le projet.
5. Ouvrez votre telephone et téléchargez l'application Expo.
6. Scannez le QR code qui s'affiche dans votre terminal.
7. L'application se lance sur votre téléphone.
8. Enjoy !

## Avancés du projet
* [x] Partie 0 - Project creation
* [x] Partie 1 - Project organisation
* [X] Partie 2 - Typescript types
* [X] Partie 3 - Screens
* [X] Partie 4 - Components
* [X] Partie 5 - FlatList
* [X] Partie 6 - Safe Area
* [X] Partie 7 - Navigation
* [X] Partie 8 - Hooks
* [X] Partie 9 - Redux Store
* [X] Partie 10 - Async Storage
* [X] Partie 11 - Theming
* [ ] Partie 12 - Unit testing
* [ ] Partie 13 - Resources


## Fabriqué avec
* [Visual Studio Code](https://code.visualstudio.com/) - Environnement de développement intégré (IDE) recommandé pour le projet multiplateforme
* [CodeFirst](https://codefirst.iut.uca.fr/) - Gitea


## Contributeurs
* [Antoine PEREDERII](https://codefirst.iut.uca.fr/git/antoine.perederii)

## Comment contribuer
1. Forkez le projet (<https://codefirst.iut.uca.fr/git/antoine.perederii/EntityFramework>)
2. Créez votre branche (`git checkout -b feature/featureName`)
3. commit vos changements (`git commit -am 'Add some feature'`)
4. Push sur la branche (`git push origin feature/featureName`)
5. Créez une nouvelle Pull Request

## License
Ce projet utilise la licence MIT - voir le fichier [LICENSE](LICENSE.md) pour plus d'informations.

## Remerciements
Ce projet a été réalisé dans le cadre des tp de la matière EntityFramework. Je remercie mon professeur pour son aide.